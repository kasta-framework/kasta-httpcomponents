package org.kastaframework.httpclient;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Http client base. This base class
 * uses spring rest template.
 *
 * @author Sabri Onur Tuzun
 * @since 11/7/14 11:19 AM
 */
public abstract class AbstractHttpClient implements HttpClient {

    protected String baseUrl;
    protected RestTemplate restTemplate;

    /**
     * Default constructor.
     *
     * @param baseUrl      base url
     * @param restTemplate rest template
     */
    protected AbstractHttpClient(String baseUrl, RestTemplate restTemplate) {
        this.baseUrl = baseUrl;
        this.restTemplate = restTemplate;
    }

    /**
     * Constructs http client without base url.
     *
     * @param restTemplate rest template
     */
    protected AbstractHttpClient(RestTemplate restTemplate) {
        this(StringUtils.EMPTY, restTemplate);
    }

    /**
     * Returns rest template.
     *
     * @return rest template
     */
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * Returns base url.
     *
     * @return base url
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Sets base url.
     *
     * @param baseUrl base url
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * Invokes http get method.
     *
     * @param url          url
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response class type
     * @return response
     */
    @Override
    public <T> ResponseEntity<T> get(String url, Class<T> responseType, Object... urlVariables) {
        return restTemplate.getForEntity(baseUrl + url, responseType, urlVariables);
    }

    /**
     * Invokes http post method.
     *
     * @param url          url
     * @param request      request
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response class type
     * @return response
     */
    @Override
    public <T> ResponseEntity<T> post(String url, Object request, Class<T> responseType, Object... urlVariables) {
        return restTemplate.postForEntity(baseUrl + url, request, responseType, urlVariables);
    }

    /**
     * Invokes http put method.
     *
     * @param url          url
     * @param request      request
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response class type
     * @return response
     */
    @Override
    public <T> ResponseEntity<T> put(String url, Object request, Class<T> responseType, Object... urlVariables) {
        return exchange(baseUrl + url, HttpMethod.PUT, request, responseType, urlVariables);
    }

    /**
     * Invokes http delete method.
     *
     * @param url          url
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response class type
     * @return response
     */
    @Override
    public <T> ResponseEntity<T> delete(String url, Class<T> responseType, Object... urlVariables) {
        return exchange(baseUrl + url, HttpMethod.DELETE, null, responseType, urlVariables);
    }

    /**
     * Invokes http method.
     *
     * @param url          url
     * @param httpMethod   http method
     * @param request      request
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response type class
     * @return response
     */
    @Override
    public <T> ResponseEntity<T> exchange(String url, HttpMethod httpMethod, Object request, Class<T> responseType, Object... urlVariables) {
        HttpEntity<?> requestEntity = new HttpEntity<>(request);
        return restTemplate.exchange(baseUrl + url, httpMethod, requestEntity, responseType, urlVariables);
    }
}
