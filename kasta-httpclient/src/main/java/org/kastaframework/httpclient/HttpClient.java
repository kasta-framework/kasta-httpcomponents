package org.kastaframework.httpclient;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * Http client interface.
 *
 * @author Sabri Onur Tuzun
 * @since 11/7/14 1:33 PM
 */
public interface HttpClient {

    /**
     * Sets base url.
     *
     * @param baseUrl base url
     */
    void setBaseUrl(String baseUrl);

    /**
     * Handles http get method.
     *
     * @param url          url
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response type class
     * @return response
     */
    <T> ResponseEntity<T> get(String url, Class<T> responseType, Object... urlVariables);

    /**
     * Handles http post method.
     *
     * @param url          url
     * @param request      request
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response type class
     * @return response
     */
    <T> ResponseEntity<T> post(String url, Object request, Class<T> responseType, Object... urlVariables);

    /**
     * Handles http put method.
     *
     * @param url          url
     * @param request      request
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response type class
     * @return response
     */
    <T> ResponseEntity<T> put(String url, Object request, Class<T> responseType, Object... urlVariables);

    /**
     * Handles http delete method.
     *
     * @param url          url
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response type class
     * @return response
     */
    <T> ResponseEntity<T> delete(String url, Class<T> responseType, Object... urlVariables);

    /**
     * Calls http method.
     *
     * @param url          url
     * @param httpMethod   http method
     * @param request      request
     * @param responseType response type
     * @param urlVariables url variables
     * @param <T>          response type class
     * @return response
     */
    <T> ResponseEntity<T> exchange(String url, HttpMethod httpMethod, Object request, Class<T> responseType, Object... urlVariables);
}
