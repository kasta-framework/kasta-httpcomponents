package org.kastaframework.httpclient;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * Httply.
 *
 * @author Sabri Onur Tuzun
 * @since 11/7/14 7:54 PM
 */
public class Httply {

    private HttpClient httpClient;
    private HttpRequestBuilder requestBuilder;

    /**
     * Constructor with http client.
     *
     * @param httpClient     http client
     * @param requestBuilder request builder
     */
    private Httply(HttpClient httpClient, HttpRequestBuilder requestBuilder) {
        this.httpClient = httpClient;
        this.requestBuilder = requestBuilder;
    }

    /**
     * Constructs httply with static access.
     *
     * @param httpClient http client
     * @return instance
     */
    public static Httply newInstance(HttpClient httpClient) {
        return new Httply(httpClient, new HttpRequestBuilder());
    }

    /**
     * Sets url.
     *
     * @param url url
     * @return instance
     */
    public Httply url(String url) {
        requestBuilder.setUrl(url);
        return this;
    }

    /**
     * Sets url variables.
     *
     * @param urlVariables url variables
     * @return instance
     */
    public Httply urlVariables(Object... urlVariables) {
        requestBuilder.addUrlVariables(urlVariables);
        return this;
    }

    /**
     * Adds url variable.
     *
     * @param urlVariable url variable
     * @return instance
     */
    public Httply urlVariable(Object urlVariable) {
        requestBuilder.addUrlVariable(urlVariable);
        return this;
    }

    /**
     * Sets requestBuilder body.
     *
     * @param requestBody requestBuilder body
     * @return instance
     */
    public Httply requestBody(Object requestBody) {
        requestBuilder.setRequestBody(requestBody);
        return this;
    }

    /**
     * Sets http method.
     *
     * @param httpMethod http method
     * @return instance
     */
    public Httply httpMethod(HttpMethod httpMethod) {
        requestBuilder.setHttpMethod(httpMethod);
        return this;
    }

    /**
     * Calls for http.
     *
     * @return response entity
     */
    public <T> ResponseEntity<T> call(Class<T> responseType) {
        return httpClient.exchange(
                requestBuilder.getUrl(),
                requestBuilder.getHttpMethod(),
                requestBuilder.getRequestBody(),
                responseType,
                requestBuilder.getUrlVariablesAsArray());
    }
}
