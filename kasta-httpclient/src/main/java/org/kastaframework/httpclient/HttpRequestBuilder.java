package org.kastaframework.httpclient;

import com.google.common.collect.Lists;
import org.kastaframework.collections.ArrayHelper;
import org.kastaframework.collections.ListHelper;
import org.springframework.http.HttpMethod;

import java.util.List;

/**
 * Http request.
 *
 * @author Sabri Onur Tuzun
 * @since 11/7/14 8:04 PM
 */
public class HttpRequestBuilder {

    private String url;
    private Object requestBody;
    private HttpMethod httpMethod;
    private List<Object> urlVariables;

    /**
     * Constructor.
     */
    public HttpRequestBuilder() {
        this.urlVariables = Lists.newLinkedList();
    }

    /**
     * Returns url.
     *
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Returns request body.
     *
     * @return request body
     */
    public Object getRequestBody() {
        return requestBody;
    }

    /**
     * Sets request body.
     *
     * @param requestBody request body
     */
    public void setRequestBody(Object requestBody) {
        this.requestBody = requestBody;
    }

    /**
     * Returns http method.
     *
     * @return http method
     */
    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    /**
     * Sets http method.
     *
     * @param httpMethod http method
     */
    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    /**
     * Returns url variables.
     *
     * @return url variables
     */
    public List<Object> getUrlVariables() {
        return urlVariables;
    }

    /**
     * Sets url variables.
     *
     * @param urlVariables url variables
     */
    public void setUrlVariables(List<Object> urlVariables) {
        this.urlVariables = urlVariables;
    }

    /**
     * Adds url variables.
     *
     * @param urlVariables url variables
     */
    public void addUrlVariables(Object... urlVariables) {
        List<Object> list = ListHelper.newLinkedList(urlVariables);
        this.urlVariables.addAll(list);
    }

    /**
     * Adds url variable.
     *
     * @param urlVariable url variable
     */
    public void addUrlVariable(Object urlVariable) {
        this.urlVariables.add(urlVariable);
    }

    /**
     * Returns url variables as array.
     *
     * @return url variables array
     */
    public Object[] getUrlVariablesAsArray() {
        return ArrayHelper.toArray(urlVariables);
    }
}
