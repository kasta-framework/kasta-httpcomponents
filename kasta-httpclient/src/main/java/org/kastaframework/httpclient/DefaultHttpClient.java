package org.kastaframework.httpclient;

import org.springframework.web.client.RestTemplate;

/**
 * Simple delegating http client
 *
 * @author Sabri Onur Tuzun
 * @since 11/7/14 9:54 PM
 */
public class DefaultHttpClient extends AbstractHttpClient {

    /**
     * Base url based constructor.
     *
     * @param baseUrl base url
     */
    public DefaultHttpClient(String baseUrl) {
        super(baseUrl, new RestTemplate());
    }

    /**
     * Default constructor.
     */
    public DefaultHttpClient() {
        super(new RestTemplate());
    }
}
