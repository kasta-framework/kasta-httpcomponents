package org.kastaframework.httpcomponents.helper.uri;

import org.apache.commons.lang3.StringUtils;
import org.kastaframework.collections.ArrayHelper;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Extends org.springframework.web.utils.UriComponentsBuilder and builds
 * uri that has non empty and non null query parameters.
 *
 * @author Sabri Onur Tuzun
 * @since 2/28/13 2:40 PM
 */
public class NonEmptyParamUriComponentsBuilder extends UriComponentsBuilder {

    /**
     * Default empty constructor.
     */
    public NonEmptyParamUriComponentsBuilder() {
    }

    /**
     * Creates new instance.
     *
     * @return new instance
     */
    public static NonEmptyParamUriComponentsBuilder newInstance() {
        return new NonEmptyParamUriComponentsBuilder();
    }

    /**
     * Adds query parameter, discards null or empty parameter.
     *
     * @param name   name of the parameter
     * @param values values of the parameter
     * @return builder instance
     */
    @Override
    public UriComponentsBuilder queryParam(String name, Object... values) {
        return (StringUtils.isNotBlank(name) && ArrayHelper.isNullSafe(values)) ? super.queryParam(name, values) : this;
    }
}
