package org.kastaframework.httpcomponents.helper.uri;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;

/**
 * @author Sabri Onur Tuzun
 * @since 6/18/13 5:45 PM
 */
public class NonEmptyParamUriComponentsBuilderTest {

    private NonEmptyParamUriComponentsBuilder builder;

    @Before
    public void setUp() {
        builder = NonEmptyParamUriComponentsBuilder.newInstance();
    }

    @Test
    public void testEmptyParameter() {
        builder.uri(URI.create("http://www.metoo.im"));
        builder.queryParam("  ", "value");

        String url = builder.build().toUriString();

        Assert.assertNotNull(url);
        Assert.assertEquals("http://www.metoo.im", url);
    }

    @Test
    public void testNullParameter() {
        builder.uri(URI.create("http://www.metoo.im"));
        builder.queryParam(null, "value");

        String url = builder.build().toUriString();

        Assert.assertNotNull(url);
        Assert.assertEquals("http://www.metoo.im", url);
    }

    @Test
    public void testNullParameterValue() {
        String[] values = null;

        builder.uri(URI.create("http://www.metoo.im"));
        builder.queryParam("name", values);

        String url = builder.build().toUriString();

        Assert.assertNotNull(url);
        Assert.assertEquals("http://www.metoo.im", url);
    }

    @Test
    public void testEmptyParameterValues() {
        builder.uri(URI.create("http://www.metoo.im"));
        builder.queryParam("name", new String[]{});

        String url = builder.build().toUriString();

        Assert.assertNotNull(url);
        Assert.assertEquals("http://www.metoo.im", url);
    }

    @Test
    public void testSetQueryParameter() {
        builder.uri(URI.create("http://www.metoo.im"));
        builder.queryParam("name", "value");

        String url = builder.build().toUriString();

        Assert.assertNotNull(url);
        Assert.assertEquals("http://www.metoo.im?name=value", url);
    }

    @Test
    public void testParameterValuesContainingNullValue() {
        String[] values = new String[]{"value", null, "value2"};

        builder.uri(URI.create("http://www.metoo.im"));
        builder.queryParam("name", values);

        String url = builder.build().toUriString();

        Assert.assertNotNull(url);
        Assert.assertEquals("http://www.metoo.im", url);
    }
}
